﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace imz_production
{
  static class Program
  {
    /// <summary>
    /// Главная точка входа для приложения.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      DateTime now = DateTime.Now;
      AppVariables.StartDate = DateTime.Parse("01." + now.Month + "." + now.Year);
      AppVariables.FinishDate = DateTime.Parse(DateTime.DaysInMonth(now.Year, now.Month) + "." + now.Month + "." + now.Year);

      Application.Run(new MainForm());
    }
  }
}
