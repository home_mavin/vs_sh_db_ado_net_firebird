﻿using System;
using System.Linq;
using System.Windows.Forms;
using FirebirdSql.Data.FirebirdClient;

namespace imz_production
{
  public partial class MainForm : Form
  {
    public MainForm()
    {
      InitializeComponent();
    }

    // TODO пример выборки с группировкой
    private void LoadCustomersData()
    {
      var dbContext = AppVariables.CreateDbContext();
      // отсоединяем все загруженные объекты
      // это необходимо чтобы обновился внутренний кеш
      // при второй и последующих вызовах этого метода
      dbContext.DetachAll(dbContext.PRODUCTION);

      var productions =
        from production in dbContext.PRODUCTION
        where production.DEPARTAMENT_ID == 1
        //orderby production.PRODUCTION_DATE
        group production by new { production.PRODUCTION_DATE, production.DEPARTAMENT_ID, production.PRODUCT_ID } into g
        orderby g.Key.PRODUCTION_DATE
        select new
        {
          Name = g.Key.DEPARTAMENT_ID,
          Date = g.Key.PRODUCTION_DATE,
          Product = g.Key.PRODUCT_ID,
          Count = g.Count(),
          Sum = g.Sum(production => production.UNITS_FACT - production.UNITS_PLAN)
        };

        //select production;

      bindingSource1.DataSource = productions.ToBindingList();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      var dialog = new FormLogin();
      dialog.UserName = "SYSDBA";
      dialog.Password = "masterkey";

      //// создание объекта, для работы с файлом
      //INIManager manager = new INIManager("config.ini");

      //// получить значение по ключу name из секции main
      //string name = manager.GetPrivateString("main", "Database");

      //// записать значение по ключу age в секции main
      //manager.WritePrivateString("main", "Database", Application.StartupPath.ToString() + "\\DBIMZ.FDB");

      if (dialog.ShowDialog() == DialogResult.OK)
      {
        var dbContext = AppVariables.CreateDbContext();

        try
        {
          string s = dbContext.Database.Connection.ConnectionString;
          var builder = new FbConnectionStringBuilder(s);
          builder.UserID = dialog.UserName;
          //builder.DataSource = "localhost";
          //builder.Port = 3350
          builder.UserID = dialog.UserName;
          builder.Password = dialog.Password;
          builder.Dialect = 3;
          builder.Charset = @"UTF8";
          builder.ServerType = FbServerType.Default;
          builder.Database = Application.StartupPath.ToString() + "\\DBIMZ.FDB";

          dbContext.Database.Connection.ConnectionString = builder.ConnectionString;
          dbContext.Database.Connection.Open();
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, "Не удалось подключиться к базе данных.");
          Application.Exit();
        }
      }
      else
      {
        Application.Exit(); 
      }

      //Data

      //LoadCustomersData();

      //dataGridView1.DataSource = bindingSource1;

      //dataGridView1.Columns["NEW_FIELD"].Visible = false;
      //dataGridView1.Columns["DEPARTMENT"].HeaderText = "Цех";
      //dataGridView1.Columns["PRODUCT_TYPE"].HeaderText = "Тип продукции";
      //dataGridView1.Columns["PRODUCTION_DATE"].HeaderText = "Дата";
      //dataGridView1.Columns["UNITS_PLAN"].HeaderText = "План";
      //dataGridView1.Columns["UNITS_FACT"].HeaderText = "Факт";
    }
    public delegate void ChangeRangeDateHandler(object sender);
    public event ChangeRangeDateHandler ChangeRangeDate;
    private Form createForm(string name)
    {
      Form form = null;
      switch (name)
      {
        case "Продукция (Справочник)":
          form = new ProductForm();
          break;
        case "Подразделения (Справочник)":
          form = new DepartamentForm();
          break;
        case "Производство":
          form = new ProductionForm();
          ChangeRangeDate += delegate(object sender)
          {
            ((ProductionForm)form).LoadProductionData();
          };
          break;
        case "Статистика":
          form = new ViewForm();
          break;
      }
      return form;
    }

    private TabPage getTabPage(string caption)
    {
      TabPage newTabPage = tabControl.TabPages[caption];
      if (newTabPage == null)
      {
        tabControl.TabPages.Add(caption, caption);
        newTabPage = tabControl.TabPages[caption];
        Form form = createForm(caption);
        form.MdiParent = this;
        form.Parent = newTabPage;
        form.Dock = DockStyle.Fill;
        form.FormBorderStyle = FormBorderStyle.None;
        form.FormClosed += this.closeTab;
        form.Show();
      }
      tabControl.SelectedTab = newTabPage;
      return newTabPage;
    }

    private void closeTab(object sender, EventArgs e)
    {
      tabControl.TabPages.Remove(((TabPage)((Form)sender).Parent));
    }

    private void rangeConfigToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var dialog = new ConfigRangeDateForm();
      dialog.StartDate = AppVariables.StartDate;
      dialog.FinishDate = AppVariables.FinishDate;
      if (dialog.ShowDialog() == DialogResult.OK)
      {
        AppVariables.StartDate = dialog.StartDate;
        AppVariables.FinishDate = dialog.FinishDate;
        // надо оповестить всех подписавшихся
        if (ChangeRangeDate != null)
          ChangeRangeDate(this);
      }
    }

    private void продукцияToolStripMenuItem_Click(object sender, EventArgs e)
    {
      getTabPage("Продукция (Справочник)");
    }

    private void подразделениеToolStripMenuItem_Click(object sender, EventArgs e)
    {
      getTabPage("Подразделения (Справочник)");
    }
    private void viewToolStripMenuItem_Click(object sender, EventArgs e)
    {
      getTabPage("Статистика");
    }

    private void productionToolStripMenuItem_Click(object sender, EventArgs e)
    {
      getTabPage("Производство");
    }

    private void btnAbout_Click(object sender, EventArgs e)
    {
      Form modalForm = new AboutForm();
      modalForm.ShowDialog();
    }
  }
}
