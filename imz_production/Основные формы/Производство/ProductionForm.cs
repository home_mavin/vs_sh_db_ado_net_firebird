﻿using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Windows.Forms;
using FirebirdSql.Data.FirebirdClient;
using imz_production.Основные_формы.Производство;

namespace imz_production
{
  public partial class ProductionForm : Form
  {
    public ProductionForm()
    {
      InitializeComponent();
    }

    public void LoadProductionData()
    {
      var dbContext = AppVariables.CreateDbContext();
      // отсоединяем все загруженные объекты
      // это необходимо чтобы обнвился внутренний кеш
      // при второй и последующих вызовах этого метода
      dbContext.DetachAll(dbContext.PRODUCTION);

      // запрос на LINQ преобразуется в SQL
      var productions =
        from production in dbContext.PRODUCTION
        where (production.PRODUCTION_DATE >= AppVariables.StartDate) &&
              (production.PRODUCTION_DATE <= AppVariables.FinishDate)
        orderby production.PRODUCTION_DATE //descending
        select new ProductionView
        {
          Id = production.PRODUCTION_ID,
          Departament_Id = production.DEPARTAMENT_ID,
          Departament_Name = production.DEPARTAMENT.DEPARTAMENT_NAME,
          Product_Id = production.PRODUCT_ID,
          Product_Name = production.PRODUCT.PRODUCT_NAME,
          Date = production.PRODUCTION_DATE,
          Plan_Unit = production.UNITS_PLAN,
          Fact_Unit = production.UNITS_FACT
          //Closed = (production.CLOSED == 1) ? "Yes" : "No"
        };

      bindingSource.DataSource = productions.ToBindingList();
    }

    private void ProductionForm_Load(object sender, EventArgs e)
    {
      LoadProductionData();

      dataGridView.DataSource = bindingSource;
      dataGridView.Columns["Id"].Visible = false;
      dataGridView.Columns["Departament_Id"].Visible = false;
      dataGridView.Columns["Product_Id"].Visible = false;
      dataGridView.Columns["Departament_Name"].HeaderText = "Подразделение";
      dataGridView.Columns["Product_Name"].HeaderText = "Продукция";
      dataGridView.Columns["Date"].HeaderText = "Дата производства";
      dataGridView.Columns["Plan_Unit"].HeaderText = "План";
      dataGridView.Columns["Fact_Unit"].HeaderText = "Факт";
      //dataGridView.Columns["Closed"].Visible = false; // Закрытие периода
    }

    public ProductionView CurrentProduction
    {
      get
      {
        return (ProductionView)bindingSource.Current;
      }
    }

    private void btnAdd_Click(object sender, EventArgs e)
    {
      var dbContext = AppVariables.CreateDbContext();
      // создание нового экземпляра сущности        
      var production = (PRODUCTION)bindingSource.AddNew();
      // создаём форму для редактирования
      using (ProductionEditorForm editor = new ProductionEditorForm())
      {
        editor.Text = "Добавление продукции";
        editor.Production = production;
        // обработчик закрытия формы
        editor.FormClosing += delegate(object fSender, FormClosingEventArgs fe)
        {
          if (editor.DialogResult == DialogResult.OK)
          {
            try
            {
              // получаем новое значение генератора
              // и присваиваем его идентификатору
              production.PRODUCTION_ID = dbContext.NextValueFor("GEN_PRODUCTION_ID");
              // добавляем новый объект
              dbContext.PRODUCTION.Add(production);
              // пытаемся сохранить изменения
              dbContext.SaveChanges();
              // и обновить текущую запись
              dbContext.Refresh(RefreshMode.StoreWins, production);
            }
            catch (Exception ex)
            {
              // отображаем ошибку
              MessageBox.Show(ex.Message, "Ошибка");
              // не закрываем форму для возможности исправления ошибки
              fe.Cancel = true;
            }
          }
          else
          {
            bindingSource.CancelEdit(); 
          }
        };
        // показываем модальную форму
        editor.ShowDialog(this);
      }
    }

    private void btnEdit_Click(object sender, EventArgs e)
    {
      var dbContext = AppVariables.CreateDbContext();
      // получаем сущность
      var production = dbContext.PRODUCTION.Find(this.CurrentProduction.Id);
      // создаём форму для редактирования
      using (ProductionEditorForm editor = new ProductionEditorForm())
      {
        editor.Text = "Редактирование продукции";
        editor.Production = production;
        // обработчик закрытия формы
        // TODO нет возврата из формы
        //editor.Production.UNITS_FACT = editor.;
        editor.FormClosing += delegate(object fSender, FormClosingEventArgs fe)
        {
          if (editor.DialogResult == DialogResult.OK)
          {
            try
            {
              // пытаемся сохранить изменения
              dbContext.SaveChanges();
              dbContext.Refresh(RefreshMode.StoreWins, production);
              // обновляем все связанные контролы
              bindingSource.ResetCurrentItem();
            }
            catch (Exception ex)
            {
              // отображаем ошибку
              MessageBox.Show(ex.Message, "Ошибка");
              // не закрываем форму для возможности исправления ошибки
              fe.Cancel = true;
            }
          }
          else
          {
            bindingSource.CancelEdit();
          }
        };
        // показываем модальную форму
        editor.ShowDialog(this);
      }
      LoadProductionData();
    }

    private void toolStripButton1_Click(object sender, EventArgs e)
    {
      var dbContext = AppVariables.CreateDbContext();
      var result = MessageBox.Show("Вы уверены, что ходите удалить данный продукт?",
        "Подтверждение",
        MessageBoxButtons.YesNo,
        MessageBoxIcon.Question);
      if (result == DialogResult.Yes)
      {
        // получаем сущность 
        var production = (PRODUCTION)bindingSource.Current;
        try
        {
          dbContext.PRODUCTION.Remove(production);
          // пытаемся сохранить изменения
          dbContext.SaveChanges();
          // удаляем из связанного списка
          bindingSource.RemoveCurrent();
        }
        catch (Exception ex)
        {
          // отображаем ошибку
          MessageBox.Show(ex.Message, "Ошибка");
        }
      }
    }

    private void btnRefresh_Click(object sender, EventArgs e)
    {
      LoadProductionData();
    }

    private void btnExit_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void dataGridView_DoubleClick(object sender, EventArgs e)
    {
      this.btnEdit_Click(sender, e);
    }
    private static class DateSet
    {
      internal static DateTime StartDate { get; set; }
      internal static DateTime EndDate { get; set; }
    }
    private void createPlanToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var dbContext = AppVariables.CreateDbContext();
      // создание нового экземпляра сущности
      var production = dbContext.PRODUCTION.Create();
      var departament = dbContext.DEPARTAMENT.Create();
      var product = dbContext.PRODUCT.Create();
      // создаём форму для редактирования
      using (AddPlanEditorForm editor = new AddPlanEditorForm())
      {
        editor.Text = "Планирование производства";
        editor.Production = production;
        editor.Departament = departament;
        editor.Product = product;
        // обработчик закрытия формы
        editor.FormClosing += delegate(object fSender, FormClosingEventArgs fe)
        {
          if (editor.DialogResult == DialogResult.OK)
          {
            DateSet.StartDate = editor.StartDate;
            DateSet.EndDate = editor.FinishDate;
            var connection = dbContext.Database.Connection;
            using (var dbTransaction = connection.BeginTransaction())
            {
              string sql = "INSERT INTO PRODUCTION " +
                           "(DEPARTAMENT_ID, PRODUCT_ID, PRODUCTION_DATE, UNITS_PLAN)" +
                           " VALUES " +
                           "(@DEPARTAMENT_ID, @PRODUCT_ID, @PRODUCTION_DATE, @UNITS_PLAN)";
              //string sql = "UPDATE PRODUCTION " +
              //             "SET PRODUCT_ID = 3 " +
              //             "WHERE PRODUCT_ID = 1";
              try
              {
                // создаём параметры запросы
                var idDepartamentParam = new FbParameter("DEPARTAMENT_ID", FbDbType.Integer);
                var idProductParam = new FbParameter("PRODUCT_ID", FbDbType.Integer);
                var productionDateParam = new FbParameter("PRODUCTION_DATE", FbDbType.Date);
                var unitsPlanParam = new FbParameter("UNITS_PLAN", FbDbType.Integer);
                // создаём SQL-команду для добавления записей
                var sqlCommand = connection.CreateCommand();
                sqlCommand.CommandText = sql;
                // указываем команде какую транзакцию использовать
                sqlCommand.Transaction = dbTransaction;

                if (DateSet.EndDate < DateSet.StartDate)
                {
                  throw new Exception("Не верный порядок дат!");
                }
                if (editor.ProductCount < 0)
                {
                  throw new Exception("Не верное число продукции!");
                }
                if (product.PRODUCT_ID <= 0 || departament.DEPARTAMENT_ID <= 0)
                {
                  throw new Exception("Не задано подразделение или продукция!");
                }

                TimeSpan dateDiff = DateSet.EndDate - DateSet.StartDate;
                int dateCount = dateDiff.Days + 1;

                idDepartamentParam.Value = departament.DEPARTAMENT_ID;
                idProductParam.Value = product.PRODUCT_ID;
                unitsPlanParam.Value = Math.Round((double)(editor.ProductCount / dateCount), 0);

                sqlCommand.Parameters.Add(idDepartamentParam);
                sqlCommand.Parameters.Add(idProductParam);
                sqlCommand.Parameters.Add(productionDateParam);
                sqlCommand.Parameters.Add(unitsPlanParam);

                // подготавливаем команду
                sqlCommand.Prepare();
                for (int i = 0; i < dateCount; i++)
                {
                  productionDateParam.Value = DateSet.StartDate.AddDays(i);
                  sqlCommand.ExecuteNonQuery();
                }
                
                dbTransaction.Commit();
              }
              catch (Exception ex)
              {
                dbTransaction.Rollback();
                // отображаем ошибку
                MessageBox.Show(ex.Message, "Ошибка");
                // не закрываем форму для возможности исправления ошибки
                fe.Cancel = true;
              } 
            }
          }
          else
          {
            bindingSource.CancelEdit();
          }
        };
        // показываем модальную форму
        editor.ShowDialog(this);
      }
    }

    private void deletePlanToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var dbContext = AppVariables.CreateDbContext();
      // создание нового экземпляра сущности
      var production = dbContext.PRODUCTION.Create();
      var departament = dbContext.DEPARTAMENT.Create();
      var product = dbContext.PRODUCT.Create();
      // создаём форму для редактирования
      using (DelPlanEditorForm editor = new DelPlanEditorForm())
      {
        editor.Text = "Удаление плана производства";
        editor.Production = production;
        editor.Departament = departament;
        editor.Product = product;
        // обработчик закрытия формы
        editor.FormClosing += delegate(object fSender, FormClosingEventArgs fe)
        {
          if (editor.DialogResult == DialogResult.OK)
          {
            DateSet.StartDate = editor.StartDate;
            DateSet.EndDate = editor.FinishDate;
            var connection = dbContext.Database.Connection;
            using (var dbTransaction = connection.BeginTransaction())
            {
              string sql = "DELETE FROM PRODUCTION " +
                           "WHERE " +
                           "DEPARTAMENT_ID = @DEPARTAMENT_ID AND " +
                           "PRODUCT_ID = @PRODUCT_ID AND " +
                           "PRODUCTION_DATE >= @PRODUCTION_DATE_START AND " +
                           "PRODUCTION_DATE <= @PRODUCTION_DATE_END";
              try
              {
                // создаём параметры запросы
                var idDepartamentParam = new FbParameter("DEPARTAMENT_ID", FbDbType.Integer);
                var idProductParam = new FbParameter("PRODUCT_ID", FbDbType.Integer);
                var productionDateStartParam = new FbParameter("PRODUCTION_DATE_START", FbDbType.Date);
                var productionDateEndParam = new FbParameter("PRODUCTION_DATE_END", FbDbType.Date);
                // создаём SQL-команду для добавления записей
                var sqlCommand = connection.CreateCommand();
                sqlCommand.CommandText = sql;
                // указываем команде какую транзакцию использовать
                sqlCommand.Transaction = dbTransaction;

                if (DateSet.EndDate < DateSet.StartDate)
                {
                  throw new Exception("Не верный порядок дат!");
                }
                if (product.PRODUCT_ID <= 0 || departament.DEPARTAMENT_ID <= 0)
                {
                  throw new Exception("Не задано подразделение или продукция!");
                }

                idDepartamentParam.Value = departament.DEPARTAMENT_ID;
                idProductParam.Value = product.PRODUCT_ID;
                productionDateStartParam.Value = DateSet.StartDate;
                productionDateEndParam.Value = DateSet.EndDate;

                // подготавливаем команду
                sqlCommand.Parameters.Add(idDepartamentParam);
                sqlCommand.Parameters.Add(idProductParam);
                sqlCommand.Parameters.Add(productionDateStartParam);
                sqlCommand.Parameters.Add(productionDateEndParam);

                sqlCommand.ExecuteNonQuery();

                dbTransaction.Commit();
              }
              catch (Exception ex)
              {
                dbTransaction.Rollback();
                // отображаем ошибку
                MessageBox.Show(ex.Message, "Ошибка");
                // не закрываем форму для возможности исправления ошибки
                fe.Cancel = true;
              }
            }
          }
          else
          {
            bindingSource.CancelEdit();
          }
        };
        // показываем модальную форму
        editor.ShowDialog(this);
      }
    }
  }
}
