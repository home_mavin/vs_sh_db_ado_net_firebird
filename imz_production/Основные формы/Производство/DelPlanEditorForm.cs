﻿using System;
using System.Windows.Forms;

namespace imz_production
{
  public partial class DelPlanEditorForm : Form
  {
    public DelPlanEditorForm()
    {
      InitializeComponent();
    }
    public PRODUCTION Production { get; set; }
    public DEPARTAMENT Departament { get; set; }
    public PRODUCT Product { get; set; }

    public DateTime StartDate
    {
      get
      {
        return edtDateBegin.Value;
      }
      set
      {
        edtDateBegin.Value = value;
      }
    }

    public DateTime FinishDate
    {
      get
      {
        return edtDateEnd.Value;
      }
      set
      {
        edtDateEnd.Value = value;
      }
    }

    private void AddPlanEditorForm_Load(object sender, EventArgs e)
    {
      //lblName.DataBindings.Add("Text", this.Productionion, "PRODUCT.PRODUCT_NAME");
      //lblUnitsPlan.DataBindings.Add("Text", this.Productionion, "UNITS_PLAN");
      //edtUnitsFact.DataBindings.Add("Text", this.Production, "UNITS_FACT");

      /*
      if (this.Departament.PRODUCTION != null)
      {
        edtDepartament.Text = this.Departament.DEPARTAMENT_NAME;
        btnDepartamentChoose.Click -= this.btnDepartamentChoose_Click;
      }
      */

      DateTime now = DateTime.Now;
      int Month = now.Month;
      int Year = now.Year;
      if (Month == 12)
      {
        ++Year;
        Month = 1;
      }
      else
      {
        ++Month;
      }
      edtDateBegin.Value = DateTime.Parse("01." + Month + "." + Year);
      edtDateEnd.Value = DateTime.Parse(DateTime.DaysInMonth(Year, Month) + "." + Month + "." + Year);
    }

    private void btnDepartamentChoose_Click(object sender, EventArgs e)
    {
      DepartamentForm departamentForm = new DepartamentForm();
      if (departamentForm.ShowDialog() == DialogResult.OK)
      {
        Departament.DEPARTAMENT_ID = departamentForm.CurrentDepartament.DEPARTAMENT_ID;
        edtDepartament.Text = departamentForm.CurrentDepartament.DEPARTAMENT_NAME;
      }
    }

    private void btnProductChoose_Click(object sender, EventArgs e)
    {
      ProductForm departamentForm = new ProductForm();
      if (departamentForm.ShowDialog() == DialogResult.OK)
      {
        Product.PRODUCT_ID = departamentForm.CurrentProduct.PRODUCT_ID;
        edtProduct.Text = departamentForm.CurrentProduct.PRODUCT_NAME;
      }
    }

    private void DelPlanEditorForm_FormClosing(object sender, FormClosingEventArgs e)
    {

    }
  }
}
