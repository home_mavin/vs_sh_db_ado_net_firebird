﻿using System;
using System.Windows.Forms;

namespace imz_production
{
  public partial class ProductionEditorForm : Form
  {
    public ProductionEditorForm()
    {
      InitializeComponent();
    }
    public PRODUCTION Production { get; set; }

    private void ProductionEditorForm_Load(object sender, EventArgs e)
    {
      lblDepartament.DataBindings.Add("Text", this.Production, "DEPARTAMENT.DEPARTAMENT_NAME");
      lblDate.DataBindings.Add("Text", this.Production, "PRODUCTION_DATE");
      lblName.DataBindings.Add("Text", this.Production, "PRODUCT.PRODUCT_NAME");
      edtUnitsPlan.DataBindings.Add("Text", this.Production, "UNITS_PLAN");
      edtUnitsFact.DataBindings.Add("Text", this.Production, "UNITS_FACT");
    }
  }
}
