﻿namespace imz_production
{
  partial class AddPlanEditorForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.edtDateEnd = new System.Windows.Forms.DateTimePicker();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.edtUnitsFact = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnOK = new System.Windows.Forms.Button();
      this.edtDateBegin = new System.Windows.Forms.DateTimePicker();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.btnDepartamentChoose = new System.Windows.Forms.Button();
      this.btnProductionChoose = new System.Windows.Forms.Button();
      this.edtDepartament = new System.Windows.Forms.TextBox();
      this.edtProduct = new System.Windows.Forms.TextBox();
      this.tableLayoutPanel1.SuspendLayout();
      this.flowLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.76923F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.23077F));
      this.tableLayoutPanel1.Controls.Add(this.edtDateEnd, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.label2, 0, 8);
      this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.edtUnitsFact, 0, 9);
      this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 10);
      this.tableLayoutPanel1.Controls.Add(this.edtDateBegin, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
      this.tableLayoutPanel1.Controls.Add(this.label5, 0, 6);
      this.tableLayoutPanel1.Controls.Add(this.btnDepartamentChoose, 1, 5);
      this.tableLayoutPanel1.Controls.Add(this.btnProductionChoose, 1, 7);
      this.tableLayoutPanel1.Controls.Add(this.edtDepartament, 0, 5);
      this.tableLayoutPanel1.Controls.Add(this.edtProduct, 0, 7);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
      this.tableLayoutPanel1.RowCount = 11;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.Size = new System.Drawing.Size(384, 329);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // edtDateEnd
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.edtDateEnd, 2);
      this.edtDateEnd.Dock = System.Windows.Forms.DockStyle.Fill;
      this.edtDateEnd.Location = new System.Drawing.Point(13, 85);
      this.edtDateEnd.Name = "edtDateEnd";
      this.edtDateEnd.Size = new System.Drawing.Size(358, 20);
      this.edtDateEnd.TabIndex = 14;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label2.Location = new System.Drawing.Point(13, 225);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(288, 13);
      this.label2.TabIndex = 11;
      this.label2.Text = "Плановое производство на период (ед.)";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label3.Location = new System.Drawing.Point(13, 21);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(288, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Начальная дата";
      // 
      // edtUnitsFact
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.edtUnitsFact, 2);
      this.edtUnitsFact.Dock = System.Windows.Forms.DockStyle.Fill;
      this.edtUnitsFact.Location = new System.Drawing.Point(13, 241);
      this.edtUnitsFact.Name = "edtUnitsFact";
      this.edtUnitsFact.Size = new System.Drawing.Size(358, 20);
      this.edtUnitsFact.TabIndex = 4;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label1.Location = new System.Drawing.Point(13, 69);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(288, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Конечная дата";
      // 
      // flowLayoutPanel1
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 2);
      this.flowLayoutPanel1.Controls.Add(this.btnCancel);
      this.flowLayoutPanel1.Controls.Add(this.btnOK);
      this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
      this.flowLayoutPanel1.Location = new System.Drawing.Point(13, 275);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
      this.flowLayoutPanel1.Size = new System.Drawing.Size(358, 41);
      this.flowLayoutPanel1.TabIndex = 0;
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(270, 8);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.Text = "Отм&ена";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // btnOK
      // 
      this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOK.Location = new System.Drawing.Point(189, 8);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 0;
      this.btnOK.Text = "&OK";
      this.btnOK.UseVisualStyleBackColor = true;
      // 
      // edtDateBegin
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.edtDateBegin, 2);
      this.edtDateBegin.Dock = System.Windows.Forms.DockStyle.Fill;
      this.edtDateBegin.Location = new System.Drawing.Point(13, 37);
      this.edtDateBegin.Name = "edtDateBegin";
      this.edtDateBegin.Size = new System.Drawing.Size(358, 20);
      this.edtDateBegin.TabIndex = 13;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label4.Location = new System.Drawing.Point(13, 117);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(288, 13);
      this.label4.TabIndex = 15;
      this.label4.Text = "Подразделение";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label5.Location = new System.Drawing.Point(13, 171);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(288, 13);
      this.label5.TabIndex = 16;
      this.label5.Text = "Вид продукции";
      // 
      // btnDepartamentChoose
      // 
      this.btnDepartamentChoose.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnDepartamentChoose.Location = new System.Drawing.Point(307, 133);
      this.btnDepartamentChoose.Name = "btnDepartamentChoose";
      this.btnDepartamentChoose.Size = new System.Drawing.Size(64, 24);
      this.btnDepartamentChoose.TabIndex = 17;
      this.btnDepartamentChoose.Text = "Выбрать";
      this.btnDepartamentChoose.UseVisualStyleBackColor = true;
      this.btnDepartamentChoose.Click += new System.EventHandler(this.btnDepartamentChoose_Click);
      // 
      // btnProductionChoose
      // 
      this.btnProductionChoose.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnProductionChoose.Location = new System.Drawing.Point(307, 187);
      this.btnProductionChoose.Name = "btnProductionChoose";
      this.btnProductionChoose.Size = new System.Drawing.Size(64, 24);
      this.btnProductionChoose.TabIndex = 18;
      this.btnProductionChoose.Text = "Выбрать";
      this.btnProductionChoose.UseVisualStyleBackColor = true;
      this.btnProductionChoose.Click += new System.EventHandler(this.btnProductChoose_Click);
      // 
      // edtDepartament
      // 
      this.edtDepartament.Dock = System.Windows.Forms.DockStyle.Top;
      this.edtDepartament.Location = new System.Drawing.Point(13, 133);
      this.edtDepartament.Name = "edtDepartament";
      this.edtDepartament.Size = new System.Drawing.Size(288, 20);
      this.edtDepartament.TabIndex = 19;
      // 
      // edtProduct
      // 
      this.edtProduct.Dock = System.Windows.Forms.DockStyle.Top;
      this.edtProduct.Location = new System.Drawing.Point(13, 187);
      this.edtProduct.Name = "edtProduct";
      this.edtProduct.Size = new System.Drawing.Size(288, 20);
      this.edtProduct.TabIndex = 20;
      // 
      // AddPlanEditorForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(384, 329);
      this.Controls.Add(this.tableLayoutPanel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "AddPlanEditorForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Планирование производства";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddPlanEditorForm_FormClosing);
      this.Load += new System.EventHandler(this.AddPlanEditorForm_Load);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.flowLayoutPanel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.TextBox edtUnitsFact;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.DateTimePicker edtDateEnd;
    private System.Windows.Forms.DateTimePicker edtDateBegin;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Button btnDepartamentChoose;
    private System.Windows.Forms.Button btnProductionChoose;
    private System.Windows.Forms.TextBox edtDepartament;
    private System.Windows.Forms.TextBox edtProduct;
  }
}