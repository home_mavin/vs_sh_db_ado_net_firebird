﻿using System;
using System.Linq;

namespace imz_production.Основные_формы.Производство
{
  public class ProductionView
  {
    public int Id { get; set; }
    public int Departament_Id { get; set; }
    public string Departament_Name { get; set; }
    public int Product_Id { get; set; }
    public string Product_Name { get; set; }
    public DateTime? Date { get; set; }
    public int Plan_Unit { get; set; }
    public int Fact_Unit { get; set; }
    //public string Closed { get; set; } // Закрытие периода

    public void Load(int Id)
    {
      var dbContext = AppVariables.CreateDbContext();

      var productions =
        from production in dbContext.PRODUCTION
        where production.PRODUCTION_ID == Id
        select new ProductionView
        {
          Id = production.PRODUCTION_ID,
          Departament_Id = production.DEPARTAMENT_ID,
          Departament_Name = production.DEPARTAMENT.DEPARTAMENT_NAME,
          Product_Id = production.PRODUCT_ID,
          Product_Name = production.PRODUCT.PRODUCT_NAME,
          Date = production.PRODUCTION_DATE,
          Plan_Unit = production.UNITS_PLAN,
          Fact_Unit = production.UNITS_FACT,
          //Closed = (production.CLOSED == 1) ? "Yes" : "No"
        };

      ProductionView productionView = productions.ToList().First();
      this.Id = productionView.Id;
      this.Departament_Id = productionView.Departament_Id;
      this.Product_Id = productionView.Product_Id;
      this.Date = productionView.Date;
      this.Plan_Unit = productionView.Plan_Unit;
      this.Fact_Unit = productionView.Fact_Unit;
      //this.Closed = invoiceView.CLOSED;
    }
  }
  public class ProductionLineView
  {
    public int Id { get; set; }
    public int Invoice_Id { get; set; }
    public int Product_Id { get; set; }
    public string Product { get; set; }
    public decimal Quantity { get; set; }
    public decimal Price { get; set; }
    public decimal Total { get; set; }
  }
}
