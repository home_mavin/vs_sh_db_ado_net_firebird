﻿namespace imz_production
{
  partial class ProductionEditorForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.lblName = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.edtUnitsFact = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnOK = new System.Windows.Forms.Button();
      this.label4 = new System.Windows.Forms.Label();
      this.lblDepartament = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.lblDate = new System.Windows.Forms.Label();
      this.edtUnitsPlan = new System.Windows.Forms.TextBox();
      this.tableLayoutPanel1.SuspendLayout();
      this.flowLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.lblName, 0, 5);
      this.tableLayoutPanel1.Controls.Add(this.label2, 1, 7);
      this.tableLayoutPanel1.Controls.Add(this.label3, 0, 4);
      this.tableLayoutPanel1.Controls.Add(this.edtUnitsFact, 0, 9);
      this.tableLayoutPanel1.Controls.Add(this.label1, 0, 6);
      this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 10);
      this.tableLayoutPanel1.Controls.Add(this.label4, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.lblDepartament, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.lblDate, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.edtUnitsPlan, 0, 7);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
      this.tableLayoutPanel1.RowCount = 11;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.Size = new System.Drawing.Size(384, 315);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // lblName
      // 
      this.lblName.AutoSize = true;
      this.tableLayoutPanel1.SetColumnSpan(this.lblName, 2);
      this.lblName.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblName.Location = new System.Drawing.Point(13, 141);
      this.lblName.Name = "lblName";
      this.lblName.Size = new System.Drawing.Size(358, 13);
      this.lblName.TabIndex = 12;
      this.lblName.Text = "lblName";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label2.Location = new System.Drawing.Point(13, 213);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(176, 13);
      this.label2.TabIndex = 11;
      this.label2.Text = "Фактическое производство (ед.)";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label3.Location = new System.Drawing.Point(13, 117);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(176, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Продукция";
      // 
      // edtUnitsFact
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.edtUnitsFact, 2);
      this.edtUnitsFact.Dock = System.Windows.Forms.DockStyle.Fill;
      this.edtUnitsFact.Location = new System.Drawing.Point(13, 229);
      this.edtUnitsFact.Name = "edtUnitsFact";
      this.edtUnitsFact.Size = new System.Drawing.Size(358, 20);
      this.edtUnitsFact.TabIndex = 4;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label1.Location = new System.Drawing.Point(13, 165);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(176, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Плановое производство (ед.)";
      // 
      // flowLayoutPanel1
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 2);
      this.flowLayoutPanel1.Controls.Add(this.btnCancel);
      this.flowLayoutPanel1.Controls.Add(this.btnOK);
      this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
      this.flowLayoutPanel1.Location = new System.Drawing.Point(13, 261);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
      this.flowLayoutPanel1.Size = new System.Drawing.Size(358, 41);
      this.flowLayoutPanel1.TabIndex = 0;
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(270, 8);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.Text = "Отм&ена";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // btnOK
      // 
      this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOK.Location = new System.Drawing.Point(189, 8);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 0;
      this.btnOK.Text = "&OK";
      this.btnOK.UseVisualStyleBackColor = true;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label4.Location = new System.Drawing.Point(13, 21);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(176, 13);
      this.label4.TabIndex = 13;
      this.label4.Text = "Подразделение";
      // 
      // lblDepartament
      // 
      this.lblDepartament.AutoSize = true;
      this.lblDepartament.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.lblDepartament.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblDepartament.Location = new System.Drawing.Point(13, 45);
      this.lblDepartament.Name = "lblDepartament";
      this.lblDepartament.Size = new System.Drawing.Size(176, 13);
      this.lblDepartament.TabIndex = 14;
      this.lblDepartament.Text = "lblDepartament";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label5.Location = new System.Drawing.Point(13, 69);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(176, 13);
      this.label5.TabIndex = 15;
      this.label5.Text = "Дата";
      // 
      // lblDate
      // 
      this.lblDate.AutoSize = true;
      this.lblDate.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblDate.Location = new System.Drawing.Point(13, 93);
      this.lblDate.Name = "lblDate";
      this.lblDate.Size = new System.Drawing.Size(176, 13);
      this.lblDate.TabIndex = 16;
      this.lblDate.Text = "lblDate";
      // 
      // edtUnitsPlan
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.edtUnitsPlan, 2);
      this.edtUnitsPlan.Dock = System.Windows.Forms.DockStyle.Fill;
      this.edtUnitsPlan.Location = new System.Drawing.Point(13, 181);
      this.edtUnitsPlan.Name = "edtUnitsPlan";
      this.edtUnitsPlan.Size = new System.Drawing.Size(358, 20);
      this.edtUnitsPlan.TabIndex = 17;
      // 
      // ProductionEditorForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(384, 315);
      this.Controls.Add(this.tableLayoutPanel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "ProductionEditorForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Редактор производства";
      this.Load += new System.EventHandler(this.ProductionEditorForm_Load);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.flowLayoutPanel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.TextBox edtUnitsFact;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lblName;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label lblDepartament;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label lblDate;
    private System.Windows.Forms.TextBox edtUnitsPlan;
  }
}