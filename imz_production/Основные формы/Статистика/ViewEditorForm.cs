﻿using System;
using System.Windows.Forms;

namespace imz_production
{
  public partial class ViewEditorForm : Form
  {
    public ViewEditorForm()
    {
      InitializeComponent();
    }
    public PRODUCTION View { get; set; }

    private void ViewEditorForm_Load(object sender, EventArgs e)
    {
      edtName.DataBindings.Add("Text", this.View, "PRODUCTION_ID");
    }
  }
}
