﻿namespace imz_production
{
  partial class ViewForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewForm));
      System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
      System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
      System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
      System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
      System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
      this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.dataGridStatistic = new System.Windows.Forms.DataGridView();
      this.toolStrip = new System.Windows.Forms.ToolStrip();
      this.btnRefresh = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.btnExit = new System.Windows.Forms.ToolStripButton();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
      this.btnRange = new System.Windows.Forms.Button();
      this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
      this.label1 = new System.Windows.Forms.Label();
      this.edtDepartament = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.edtProduct = new System.Windows.Forms.TextBox();
      this.btnDepartamentChoose = new System.Windows.Forms.Button();
      this.btnProductionChoose = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.edtDiff = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.dataGridView = new System.Windows.Forms.DataGridView();
      ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridStatistic)).BeginInit();
      this.toolStrip.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel2);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
      this.splitContainer1.Size = new System.Drawing.Size(689, 691);
      this.splitContainer1.SplitterDistance = 189;
      this.splitContainer1.TabIndex = 4;
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 1;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel2.Controls.Add(this.dataGridStatistic, 0, 1);
      this.tableLayoutPanel2.Controls.Add(this.toolStrip, 0, 0);
      this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 2;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(689, 189);
      this.tableLayoutPanel2.TabIndex = 17;
      // 
      // dataGridStatistic
      // 
      this.dataGridStatistic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridStatistic.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dataGridStatistic.Location = new System.Drawing.Point(3, 28);
      this.dataGridStatistic.Name = "dataGridStatistic";
      this.dataGridStatistic.ReadOnly = true;
      this.dataGridStatistic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dataGridStatistic.Size = new System.Drawing.Size(683, 158);
      this.dataGridStatistic.TabIndex = 20;
      // 
      // toolStrip
      // 
      this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRefresh,
            this.toolStripSeparator2,
            this.btnExit});
      this.toolStrip.Location = new System.Drawing.Point(0, 0);
      this.toolStrip.Name = "toolStrip";
      this.toolStrip.Size = new System.Drawing.Size(689, 25);
      this.toolStrip.TabIndex = 16;
      this.toolStrip.Text = "toolStrip1";
      // 
      // btnRefresh
      // 
      this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
      this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnRefresh.Name = "btnRefresh";
      this.btnRefresh.Size = new System.Drawing.Size(65, 22);
      this.btnRefresh.Text = "&Обновить";
      this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
      // 
      // btnExit
      // 
      this.btnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
      this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(57, 22);
      this.btnExit.Text = "&Закрыть";
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 3;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 177F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 31F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.Controls.Add(this.dateTimePicker, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.btnRange, 0, 9);
      this.tableLayoutPanel1.Controls.Add(this.chart1, 2, 0);
      this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.edtDepartament, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.label2, 0, 4);
      this.tableLayoutPanel1.Controls.Add(this.edtProduct, 0, 5);
      this.tableLayoutPanel1.Controls.Add(this.btnDepartamentChoose, 1, 3);
      this.tableLayoutPanel1.Controls.Add(this.btnProductionChoose, 1, 5);
      this.tableLayoutPanel1.Controls.Add(this.label3, 0, 6);
      this.tableLayoutPanel1.Controls.Add(this.edtDiff, 0, 7);
      this.tableLayoutPanel1.Controls.Add(this.label4, 0, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
      this.tableLayoutPanel1.RowCount = 10;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(689, 498);
      this.tableLayoutPanel1.TabIndex = 5;
      // 
      // dateTimePicker
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.dateTimePicker, 2);
      this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dateTimePicker.Location = new System.Drawing.Point(8, 28);
      this.dateTimePicker.Name = "dateTimePicker";
      this.dateTimePicker.Size = new System.Drawing.Size(200, 20);
      this.dateTimePicker.TabIndex = 8;
      this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
      // 
      // btnRange
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.btnRange, 2);
      this.btnRange.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnRange.Location = new System.Drawing.Point(8, 238);
      this.btnRange.Name = "btnRange";
      this.btnRange.Size = new System.Drawing.Size(202, 252);
      this.btnRange.TabIndex = 22;
      this.btnRange.Text = "Временной диапазон";
      this.btnRange.UseVisualStyleBackColor = true;
      this.btnRange.Visible = false;
      // 
      // chart1
      // 
      chartArea1.Name = "ChartArea1";
      this.chart1.ChartAreas.Add(chartArea1);
      this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
      legend1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Bottom;
      legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
      legend1.Name = "Legend1";
      this.chart1.Legends.Add(legend1);
      this.chart1.Location = new System.Drawing.Point(216, 8);
      this.chart1.Name = "chart1";
      this.tableLayoutPanel1.SetRowSpan(this.chart1, 10);
      series1.BorderWidth = 2;
      series1.ChartArea = "ChartArea1";
      series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
      series1.Label = "#VAL";
      series1.Legend = "Legend1";
      series1.MarkerSize = 6;
      series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
      series1.Name = "План (Накопительный)";
      series2.BorderWidth = 2;
      series2.ChartArea = "ChartArea1";
      series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
      series2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
      series2.Label = "#VAL";
      series2.Legend = "Legend1";
      series2.MarkerSize = 6;
      series2.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
      series2.Name = "Факт (Накопительный)";
      series3.BorderWidth = 2;
      series3.ChartArea = "ChartArea1";
      series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
      series3.Label = "#VAL";
      series3.Legend = "Legend1";
      series3.MarkerSize = 6;
      series3.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
      series3.Name = "Отклонение (Накопительное)";
      series3.SmartLabelStyle.CalloutLineColor = System.Drawing.Color.Maroon;
      this.chart1.Series.Add(series1);
      this.chart1.Series.Add(series2);
      this.chart1.Series.Add(series3);
      this.chart1.Size = new System.Drawing.Size(485, 482);
      this.chart1.TabIndex = 8;
      this.chart1.Text = "chart1";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label1.Location = new System.Drawing.Point(8, 60);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(171, 13);
      this.label1.TabIndex = 12;
      this.label1.Text = "Подразделение";
      // 
      // edtDepartament
      // 
      this.edtDepartament.Dock = System.Windows.Forms.DockStyle.Fill;
      this.edtDepartament.Enabled = false;
      this.edtDepartament.Location = new System.Drawing.Point(8, 76);
      this.edtDepartament.Name = "edtDepartament";
      this.edtDepartament.Size = new System.Drawing.Size(171, 20);
      this.edtDepartament.TabIndex = 13;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label2.Location = new System.Drawing.Point(8, 114);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(171, 13);
      this.label2.TabIndex = 14;
      this.label2.Text = "Продукция";
      // 
      // edtProduct
      // 
      this.edtProduct.Dock = System.Windows.Forms.DockStyle.Fill;
      this.edtProduct.Enabled = false;
      this.edtProduct.Location = new System.Drawing.Point(8, 130);
      this.edtProduct.Name = "edtProduct";
      this.edtProduct.Size = new System.Drawing.Size(171, 20);
      this.edtProduct.TabIndex = 15;
      // 
      // btnDepartamentChoose
      // 
      this.btnDepartamentChoose.Location = new System.Drawing.Point(185, 76);
      this.btnDepartamentChoose.Name = "btnDepartamentChoose";
      this.btnDepartamentChoose.Size = new System.Drawing.Size(23, 23);
      this.btnDepartamentChoose.TabIndex = 16;
      this.btnDepartamentChoose.Text = "+";
      this.btnDepartamentChoose.UseVisualStyleBackColor = true;
      this.btnDepartamentChoose.Click += new System.EventHandler(this.btnDepartamentChoose_Click);
      // 
      // btnProductionChoose
      // 
      this.btnProductionChoose.Location = new System.Drawing.Point(185, 130);
      this.btnProductionChoose.Name = "btnProductionChoose";
      this.btnProductionChoose.Size = new System.Drawing.Size(23, 23);
      this.btnProductionChoose.TabIndex = 17;
      this.btnProductionChoose.Text = "+";
      this.btnProductionChoose.UseVisualStyleBackColor = true;
      this.btnProductionChoose.Click += new System.EventHandler(this.btnProductionChoose_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.tableLayoutPanel1.SetColumnSpan(this.label3, 2);
      this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label3.Location = new System.Drawing.Point(8, 168);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(202, 13);
      this.label3.TabIndex = 18;
      this.label3.Text = "Отклонение до начала периода (ед.)";
      // 
      // edtDiff
      // 
      this.tableLayoutPanel1.SetColumnSpan(this.edtDiff, 2);
      this.edtDiff.Dock = System.Windows.Forms.DockStyle.Fill;
      this.edtDiff.Enabled = false;
      this.edtDiff.Location = new System.Drawing.Point(8, 184);
      this.edtDiff.Name = "edtDiff";
      this.edtDiff.Size = new System.Drawing.Size(202, 20);
      this.edtDiff.TabIndex = 19;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label4.Location = new System.Drawing.Point(8, 12);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(171, 13);
      this.label4.TabIndex = 24;
      this.label4.Text = "Выбор месяца";
      // 
      // dataGridView
      // 
      this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView.Location = new System.Drawing.Point(296, 295);
      this.dataGridView.Name = "dataGridView";
      this.dataGridView.Size = new System.Drawing.Size(97, 101);
      this.dataGridView.TabIndex = 21;
      this.dataGridView.Visible = false;
      // 
      // ViewForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(689, 691);
      this.Controls.Add(this.dataGridView);
      this.Controls.Add(this.splitContainer1);
      this.Name = "ViewForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Статистика";
      this.Load += new System.EventHandler(this.ViewForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridStatistic)).EndInit();
      this.toolStrip.ResumeLayout(false);
      this.toolStrip.PerformLayout();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.BindingSource bindingSource;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.DateTimePicker dateTimePicker;
    private System.Windows.Forms.Button btnRange;
    private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox edtDepartament;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox edtProduct;
    private System.Windows.Forms.Button btnDepartamentChoose;
    private System.Windows.Forms.Button btnProductionChoose;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox edtDiff;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    private System.Windows.Forms.ToolStrip toolStrip;
    private System.Windows.Forms.ToolStripButton btnRefresh;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripButton btnExit;
    private System.Windows.Forms.DataGridView dataGridStatistic;
    private System.Windows.Forms.DataGridView dataGridView;
  }
}