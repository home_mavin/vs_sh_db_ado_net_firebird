﻿using System;
using System.Linq;
using System.Windows.Forms;
using imz_production.Основные_формы.Статистика;

namespace imz_production
{
  public partial class ViewForm : Form
  {
    public ViewForm()
    {
      InitializeComponent();
    }
    private static DateTime StartDateStat { get; set; }
    private static DateTime FinishDateStat { get; set; }
    private void LoadViewData()
    {
      var dbContext = AppVariables.CreateDbContext();
      // отсоединяем все загруженные объекты
      // это необходимо чтобы обнвился внутренний кеш
      // при второй и последующих вызовах этого метода
      dbContext.DetachAll(dbContext.PRODUCTION);

      if (LocalAppVariables.Departament == null)
        LocalAppVariables.Departament = dbContext.DEPARTAMENT.Create();
      if (LocalAppVariables.Product == null)
        LocalAppVariables.Product = dbContext.PRODUCT.Create();

      var views =
        from view in dbContext.PRODUCTION
        where (view.PRODUCTION_DATE >= StartDateStat) &&
              (view.PRODUCTION_DATE <= FinishDateStat) &&
              (view.DEPARTAMENT_ID == LocalAppVariables.Departament.DEPARTAMENT_ID) &&
              (view.PRODUCT_ID == LocalAppVariables.Product.PRODUCT_ID)
        orderby view.PRODUCTION_DATE
        select view;
      
      bindingSource.DataSource = views.ToBindingList();

      // TODO грид статистики
      dataGridStatistic.Rows.Clear();
      dataGridStatistic.Columns.Clear();
      dataGridStatistic.Columns.Add("PRODUCTION_DATE", "Дата производства");
      dataGridStatistic.Columns.Add("UNITS_PLAN", "План");
      dataGridStatistic.Columns.Add("UNITS_FACT", "Факт");
      dataGridStatistic.Columns.Add("DIFF", "Отклонение");
      dataGridStatistic.Columns.Add("DIFF_ADD", "Отклонение (накопительное)");
      int dateDayOfMonth = 1;
      int diff = 0;
      int diffAdd = 0;
      int counter = views.Count();
      foreach (PRODUCTION s in views)
      {
        while (dateDayOfMonth != s.PRODUCTION_DATE.Date.Day)
        {
          dataGridStatistic.Rows.Add(s.PRODUCTION_DATE.AddDays(dateDayOfMonth - s.PRODUCTION_DATE.Day).ToString("dd.MM.yyyy"), 0, 0, 0, diffAdd);
          dateDayOfMonth++;
        };
        diff = s.UNITS_FACT - s.UNITS_PLAN;
        diffAdd += diff;
        dataGridStatistic.Rows.Add(s.PRODUCTION_DATE.ToString("dd.MM.yyyy"), s.UNITS_PLAN, s.UNITS_FACT, diff, diffAdd);
        dateDayOfMonth++;
        if (--counter == 0)
        {
          while (dateDayOfMonth <= DateTime.DaysInMonth(s.PRODUCTION_DATE.Date.Year, s.PRODUCTION_DATE.Date.Month))
          {
            dataGridStatistic.Rows.Add(s.PRODUCTION_DATE.AddDays(dateDayOfMonth - s.PRODUCTION_DATE.Day).ToString("dd.MM.yyyy"), 0, 0, 0, diffAdd);
            dateDayOfMonth++;
          }
        }
      }

      //// TODO накопительный итог в Firebird 3.0
      //SELECT
      //  PRODUCTION_ID,
      //  SUM(UNITS_FACT - UNITS_PLAN) OVER (ORDER BY PDODUCTION_DATE) diff
      //  FROM PRODUCTION
      //  ORDER BY PDODUCTION_DATE;

      //// TODO накопительный итог в Firebird 2.5
      //SELECT PRODUCTION_DATE, UNITS_PLAN, UNITS_FACT, SUM(UNITS_FACT - UNITS_PLAN) DIFF, 
      //(SELECT SUM(UNITS_FACT - UNITS_PLAN) 
      //FROM PRODUCTION 
      //WHERE DEPARTAMENT_ID = 5 AND
      //  PRODUCT_ID = 4 AND
      //PRODUCTION_DATE <= QUERY.PRODUCTION_DATE AND 
      //PRODUCTION_DATE >= '2017.07.25') AS UNITS_SUM 
      //FROM PRODUCTION QUERY
      //WHERE DEPARTAMENT_ID = 5 AND
      //  PRODUCT_ID = 4 AND
      //PRODUCTION_DATE >= '2017.07.25' AND
      //PRODUCTION_DATE <= '2017.07.30'
      //GROUP BY PRODUCTION_DATE, UNITS_PLAN, UNITS_FACT
      //ORDER BY PRODUCTION_DATE;

      // считаем отклонения от графика за предыдущий период
      var count =
        from view in dbContext.PRODUCTION
        where (view.PRODUCTION_DATE < StartDateStat) &&
              (view.DEPARTAMENT_ID == LocalAppVariables.Departament.DEPARTAMENT_ID) &&
              (view.PRODUCT_ID == LocalAppVariables.Product.PRODUCT_ID)
        //group view by new { view.PRODUCTION_DATE } into g
        group view by new { view.DEPARTAMENT_ID } into g
        select new
        {
          Sum = g.Sum(production => production.UNITS_FACT - production.UNITS_PLAN)
        };
      edtDiff.Text = "";
      if (count.ToList().Count != 0)
      {
        var tmp = count.ToList()[0].Sum.ToString();
        edtDiff.Text = tmp;
      }

      // TODO временные правки
      if (dataGridView.RowCount > 1)
      {
        if (dataGridView.ColumnCount < 9) {
          dataGridView.Columns.Add("DIFF", "Отклонение");
          dataGridView.Columns.Add("DIFF_ADD", "Отклонение накопительное");
        }
        diff = 0;
        diffAdd = 0;
        for (int i = 0; i < dataGridView.Rows.Count; i++)
        {
          diff = Convert.ToInt32(dataGridView.Rows[i].Cells["UNITS_FACT"].Value) -
                 Convert.ToInt32(dataGridView.Rows[i].Cells["UNITS_PLAN"].Value);
          dataGridView.Rows[i].Cells["DIFF"].Value = diff;
          diffAdd += diff;
          dataGridView.Rows[i].Cells["DIFF_ADD"].Value = diffAdd;
        }
      }
    }

    private void LoadChart()
    {
      chart1.Series[0].Points.Clear();
      chart1.Series[1].Points.Clear();
      chart1.Series[2].Points.Clear();

      DateTime now = DateTime.Now;

      int curr = 0;
      int plan = 0;
      int fact = 0;
      for (int i = 0; i < dataGridStatistic.Rows.Count - 1; i++)
      {
        var date = Convert.ToString(dataGridStatistic.Rows[i].Cells["PRODUCTION_DATE"].Value);
        plan += Convert.ToInt32(dataGridStatistic.Rows[i].Cells["UNITS_PLAN"].Value);
        fact += Convert.ToInt32(dataGridStatistic.Rows[i].Cells["UNITS_FACT"].Value);
        chart1.Series[0].Points.AddXY(date, plan);
        chart1.Series[1].Points.AddXY(date, fact);
        if (now.AddDays(1) > Convert.ToDateTime(dataGridStatistic.Rows[i].Cells["PRODUCTION_DATE"].Value))
        {
          curr += Convert.ToInt32(dataGridStatistic.Rows[i].Cells["UNITS_FACT"].Value) - Convert.ToInt32(dataGridStatistic.Rows[i].Cells["UNITS_PLAN"].Value);
          chart1.Series[2].Points.AddXY(date, curr); 
        }
      }
    }

    private void ViewForm_Load(object sender, EventArgs e)
    {
      setDataSetDateTime();
      LoadViewData();

      dataGridView.DataSource = bindingSource;
      //dataGridView.Columns["PRODUCTION_ID"].HeaderText = "Идентификатор";
      //dataGridView.Columns["DEPARTAMENT_ID"].HeaderText = "Идентификатор подразделения";
      //dataGridView.Columns["PRODUCT_ID"].HeaderText = "Идентификатор продукции";
      dataGridView.Columns["PRODUCTION_ID"].Visible = false;
      dataGridView.Columns["DEPARTAMENT_ID"].Visible = false;
      dataGridView.Columns["PRODUCT_ID"].Visible = false;
      dataGridView.Columns["PRODUCTION_DATE"].HeaderText = "Дата производства";
      dataGridView.Columns["UNITS_PLAN"].HeaderText = "План";
      dataGridView.Columns["UNITS_FACT"].HeaderText = "Факт";
      dataGridView.Columns["PRODUCT"].Visible = false;
      dataGridView.Columns["DEPARTAMENT"].Visible = false;

      dateTimePicker.Format = DateTimePickerFormat.Custom;
      dateTimePicker.CustomFormat = "MMMM yyyy";

      LoadChart();
    }

    public PRODUCTION CurrentView
    {
      get
      {
        return (PRODUCTION)bindingSource.Current;
      }
    }

    private void btnRefresh_Click(object sender, EventArgs e)
    {
      LoadViewData();
      LoadChart();
    }

    private void btnExit_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      var dialog = new ConfigRangeDateForm();
      dialog.StartDate = AppVariables.StartDate;
      dialog.FinishDate = AppVariables.FinishDate;
      if (dialog.ShowDialog() == DialogResult.OK)
      {
        AppVariables.StartDate = dialog.StartDate;
        AppVariables.FinishDate = dialog.FinishDate;
      }
      LoadViewData();
      LoadChart();
    }

    private void dataGridView_MouseUp(object sender, MouseEventArgs e)
    {
      LoadChart();
    }

    private void setDataSetDateTime()
    {
      DateTime now = dateTimePicker.Value;

      StartDateStat = DateTime.Parse("01." + now.Month + "." + now.Year);
      FinishDateStat = DateTime.Parse(DateTime.DaysInMonth(now.Year, now.Month) + "." + now.Month + "." + now.Year);
    }

    private void dateTimePicker_ValueChanged(object sender, EventArgs e)
    {
      setDataSetDateTime();
      LoadViewData();
      LoadChart();
    }

    private void btnRefreshChart_Click(object sender, EventArgs e)
    {
      LoadChart();
    }

    private void btnDepartamentChoose_Click(object sender, EventArgs e)
    {
      DepartamentForm departamentForm = new DepartamentForm();
      if (departamentForm.ShowDialog() == DialogResult.OK)
      {
        LocalAppVariables.Departament.DEPARTAMENT_ID = departamentForm.CurrentDepartament.DEPARTAMENT_ID;
        edtDepartament.Text = departamentForm.CurrentDepartament.DEPARTAMENT_NAME;
      }
      LoadViewData();
      LoadChart();
    }

    private void btnProductionChoose_Click(object sender, EventArgs e)
    {
      ProductForm departamentForm = new ProductForm();
      if (departamentForm.ShowDialog() == DialogResult.OK)
      {
        LocalAppVariables.Product.PRODUCT_ID = departamentForm.CurrentProduct.PRODUCT_ID;
        edtProduct.Text = departamentForm.CurrentProduct.PRODUCT_NAME;
      }
      LoadViewData();
      LoadChart();
    }
  }
}