﻿using System;
using System.Windows.Forms;

namespace imz_production
{
  public partial class FormLogin : Form
  {
    public FormLogin()
    {
      InitializeComponent();
    }
    public string UserName
    {
      get
      {
        return edtUserName.Text;
      }
      set
      {
        edtUserName.Text = value;
      }
    }

    public string Password
    {
      get
      {
        return edtPassword.Text;
      }
      set
      {
        edtPassword.Text = value;
      }
    }
    private void btnOk_Click(object sender, EventArgs e)
    {

    }
  }
}
