﻿using System;
using System.Windows.Forms;

namespace imz_production
{
  public partial class ProductEditorForm : Form
  {
    public ProductEditorForm()
    {
      InitializeComponent();
    }
    public PRODUCT Product { get; set; }

    private void ProductEditorForm_Load(object sender, EventArgs e)
    {
      edtName.DataBindings.Add("Text", this.Product, "PRODUCT_NAME");
    }
  }
}
