﻿using System;
using System.Windows.Forms;

namespace imz_production
{
  public partial class DepartamentEditorForm : Form
  {
    public DepartamentEditorForm()
    {
      InitializeComponent();
    }
    public DEPARTAMENT Departament { get; set; }

    private void DepartamentEditorForm_Load(object sender, EventArgs e)
    {
      edtName.DataBindings.Add("Text", this.Departament, "DEPARTAMENT_NAME");
    }
  }
}
