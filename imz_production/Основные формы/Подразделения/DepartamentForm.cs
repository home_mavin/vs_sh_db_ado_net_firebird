﻿using System;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace imz_production
{
  public partial class DepartamentForm : Form
  {
    public DepartamentForm()
    {
      InitializeComponent();
    }

    private void LoadDepartamentData()
    {
      var dbContext = AppVariables.CreateDbContext();
      // отсоединяем все загруженные объекты
      // это необходимо чтобы обнвился внутренний кеш
      // при второй и последующих вызовах этого метода
      dbContext.DetachAll(dbContext.DEPARTAMENT);

      var views =
        from view in dbContext.DEPARTAMENT
        orderby view.DEPARTAMENT_NAME
        select view;

      bindingSource.DataSource = views.ToBindingList();
    }

    private void DepartamentForm_Load(object sender, EventArgs e)
    {
      LoadDepartamentData();

      dataGridView.DataSource = bindingSource;
      dataGridView.Columns["DEPARTAMENT_ID"].Visible = false;
      dataGridView.Columns["DEPARTAMENT_NAME"].HeaderText = "Наименование";
      dataGridView.Columns["PRODUCTION"].Visible = false;
      dataGridView.Columns["DEPARTAMENT_CODE"].HeaderText = "Код подразделения";
    }

    public DEPARTAMENT CurrentDepartament
    {
      get
      {
        return (DEPARTAMENT)bindingSource.Current;
      }
    }

    private void btnAdd_Click(object sender, EventArgs e)
    {
      var dbContext = AppVariables.CreateDbContext();
      // создание нового экземпляра сущности        
      var departament = (DEPARTAMENT)bindingSource.AddNew();
      // создаём форму для редактирования
      using (DepartamentEditorForm editor = new DepartamentEditorForm())
      {
        editor.Text = "Добавление подразделения";
        editor.Departament = departament;
        // обработчик закрытия формы
        editor.FormClosing += delegate(object fSender, FormClosingEventArgs fe)
        {
          if (editor.DialogResult == DialogResult.OK)
          {
            try
            {
              // получаем новое значение генератора
              // и присваиваем его идентификатору
              departament.DEPARTAMENT_ID = dbContext.NextValueFor("GEN_DEPARTAMENT_ID");
              // добавляем новый объект
              dbContext.DEPARTAMENT.Add(departament);
              // пытаемся сохранить изменения
              dbContext.SaveChanges();
              // и обновить текущую запись
              dbContext.Refresh(RefreshMode.StoreWins, departament);
            }
            catch (Exception ex)
            {
              // отображаем ошибку
              MessageBox.Show(ex.Message, "Ошибка");
              // не закрываем форму для возможности исправления ошибки
              fe.Cancel = true;
            }
          }
          else
          {
            bindingSource.CancelEdit(); 
          }
        };
        // показываем модальную форму
        editor.ShowDialog(this);
      }
    }

    private void btnEdit_Click(object sender, EventArgs e)
    {
      var dbContext = AppVariables.CreateDbContext();
      // получаем сущность
      var departament = (DEPARTAMENT)bindingSource.Current;
      // создаём форму для редактирования
      using (DepartamentEditorForm editor = new DepartamentEditorForm())
      {
        editor.Text = "Редактирование подразделения";
        editor.Departament = departament;
        // обработчик закрытия формы
        editor.FormClosing += delegate(object fSender, FormClosingEventArgs fe)
        {
          if (editor.DialogResult == DialogResult.OK)
          {
            try
            {
              // пытаемся сохранить изменения
              dbContext.SaveChanges();
              dbContext.Refresh(RefreshMode.StoreWins, departament);
              // обновляем все связанные контролы
              bindingSource.ResetCurrentItem();
            }
            catch (Exception ex)
            {
              // отображаем ошибку
              MessageBox.Show(ex.Message, "Ошибка");
              // не закрываем форму для возможности исправления ошибки
              fe.Cancel = true;
            }
          }
          else
          {
            bindingSource.CancelEdit();
          }
        };
        // показываем модальную форму
        editor.ShowDialog(this);
      }
    }

    private void toolStripButton1_Click(object sender, EventArgs e)
    {
      var dbContext = AppVariables.CreateDbContext();
      var result = MessageBox.Show("Вы уверены, что ходите удалить данный департамент?",
        "Подтверждение",
        MessageBoxButtons.YesNo,
        MessageBoxIcon.Question);
      if (result == DialogResult.Yes)
      {
        // получаем сущность 
        var view = (DEPARTAMENT)bindingSource.Current;
        try
        {
          dbContext.DEPARTAMENT.Remove(view);
          // пытаемся сохранить изменения
          dbContext.SaveChanges();
          // удаляем из связанного списка
          bindingSource.RemoveCurrent();
        }
        catch (Exception ex)
        {
          // отображаем ошибку
          MessageBox.Show(ex.Message, "Ошибка");
        }
      }
    }

    private void btnRefresh_Click(object sender, EventArgs e)
    {
      LoadDepartamentData();
    }

    private void btnExit_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void dataGridView_DoubleClick(object sender, EventArgs e)
    {
      this.btnEdit_Click(sender, e);
    }

    private void DepartamentForm_Shown(object sender, EventArgs e)
    {
      if (this.Modal)
      {
        this.StartPosition = FormStartPosition.CenterScreen;

        FlowLayoutPanel flowLayoutPanel = new FlowLayoutPanel();
        Button btnCancel = new Button();
        Button btnOK = new Button();

        this.Controls.Add(flowLayoutPanel);
        flowLayoutPanel.Controls.Add(btnCancel);
        flowLayoutPanel.Controls.Add(btnOK);
        flowLayoutPanel.Dock = DockStyle.Bottom;
        flowLayoutPanel.FlowDirection = FlowDirection.RightToLeft;
        flowLayoutPanel.Name = "flowLayoutPanel";
        flowLayoutPanel.Padding = new Padding(10);
        flowLayoutPanel.Size = new Size(333, 50);
        flowLayoutPanel.TabIndex = 0;

        // 
        // btnCancel
        // 
        btnCancel.DialogResult = DialogResult.Cancel;
        btnCancel.Name = "btnCancel";
        btnCancel.Size = new Size(75, 23);
        btnCancel.TabIndex = 0;
        btnCancel.Text = "Cancel";
        btnCancel.UseVisualStyleBackColor = true;
        // 
        // btnOK
        // 
        btnOK.DialogResult = DialogResult.OK;
        btnOK.Location = new Point(154, 13);
        btnOK.Name = "btnOK";
        btnOK.Size = new Size(75, 23);
        btnOK.TabIndex = 1;
        btnOK.Text = "OK";
        btnOK.UseVisualStyleBackColor = true;
      }
    }
  }
}
