﻿using System;
using System.Windows.Forms;

namespace imz_production
{
  public partial class ConfigRangeDateForm : Form
  {
    public ConfigRangeDateForm()
    {
      InitializeComponent();
    }

    public DateTime StartDate
    {
      get
      {
        return dateBegin.Value;
      }
      set
      {
        dateBegin.Value = value;
      }
    }

    public DateTime FinishDate
    {
      get
      {
        return dateEnd.Value;
      }
      set
      {
        dateEnd.Value = value;
      }
    }
  }
}
