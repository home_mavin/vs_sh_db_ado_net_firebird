namespace imz_production
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Firebird.DEPARTAMENT")]
    public partial class DEPARTAMENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DEPARTAMENT()
        {
            PRODUCTION = new HashSet<PRODUCTION>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DEPARTAMENT_ID { get; set; }

        [Required]
        [StringLength(100)]
        public string DEPARTAMENT_NAME { get; set; }

        public short? DEPARTAMENT_CODE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCTION> PRODUCTION { get; set; }
    }
}
