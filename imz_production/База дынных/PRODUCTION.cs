namespace imz_production
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Firebird.PRODUCTION")]
    public partial class PRODUCTION
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PRODUCTION_ID { get; set; }

        public int DEPARTAMENT_ID { get; set; }

        public int PRODUCT_ID { get; set; }

        [Column(TypeName = "date")]
        public DateTime PRODUCTION_DATE { get; set; }

        public int UNITS_PLAN { get; set; }

        public int UNITS_FACT { get; set; }

        public virtual DEPARTAMENT DEPARTAMENT { get; set; }

        public virtual PRODUCT PRODUCT { get; set; }
    }
}
