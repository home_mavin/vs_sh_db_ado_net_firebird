namespace imz_production
{
  using System.Data.Entity;

  public partial class DbModel : DbContext
  {
    public DbModel()
      : base("name=DbModel")
    {
    }

    public virtual DbSet<DEPARTAMENT> DEPARTAMENT { get; set; }
    public virtual DbSet<PRODUCT> PRODUCT { get; set; }
    public virtual DbSet<PRODUCTION> PRODUCTION { get; set; }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      modelBuilder.Entity<DEPARTAMENT>()
          .HasMany(e => e.PRODUCTION)
          .WithRequired(e => e.DEPARTAMENT)
          .WillCascadeOnDelete(false);

      modelBuilder.Entity<PRODUCT>()
          .HasMany(e => e.PRODUCTION)
          .WithRequired(e => e.PRODUCT)
          .WillCascadeOnDelete(false);
    }
  }
}
