﻿namespace imz_production
{
  partial class MainForm
  {
    /// <summary>
    /// Требуется переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором форм Windows

    /// <summary>
    /// Обязательный метод для поддержки конструктора - не изменяйте
    /// содержимое данного метода при помощи редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.mainMenu = new System.Windows.Forms.ToolStrip();
      this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
      this.productionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
      this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.departamentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
      this.configRangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.btnAbout = new System.Windows.Forms.ToolStripButton();
      this.tabControl = new System.Windows.Forms.TabControl();
      this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
      this.serviceController = new System.ServiceProcess.ServiceController();
      this.mainMenu.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
      this.SuspendLayout();
      // 
      // mainMenu
      // 
      this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton3,
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2,
            this.btnAbout});
      this.mainMenu.Location = new System.Drawing.Point(0, 0);
      this.mainMenu.Name = "mainMenu";
      this.mainMenu.Size = new System.Drawing.Size(723, 25);
      this.mainMenu.TabIndex = 1;
      this.mainMenu.Text = "toolStrip1";
      // 
      // toolStripDropDownButton3
      // 
      this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productionToolStripMenuItem,
            this.viewToolStripMenuItem});
      this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
      this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
      this.toolStripDropDownButton3.Size = new System.Drawing.Size(104, 22);
      this.toolStripDropDownButton3.Text = "Представление";
      // 
      // productionToolStripMenuItem
      // 
      this.productionToolStripMenuItem.Name = "productionToolStripMenuItem";
      this.productionToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.productionToolStripMenuItem.Text = "Производство";
      this.productionToolStripMenuItem.Click += new System.EventHandler(this.productionToolStripMenuItem_Click);
      // 
      // viewToolStripMenuItem
      // 
      this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
      this.viewToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.viewToolStripMenuItem.Text = "Статистика";
      this.viewToolStripMenuItem.Click += new System.EventHandler(this.viewToolStripMenuItem_Click);
      // 
      // toolStripDropDownButton1
      // 
      this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productToolStripMenuItem,
            this.departamentToolStripMenuItem});
      this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
      this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
      this.toolStripDropDownButton1.Size = new System.Drawing.Size(95, 22);
      this.toolStripDropDownButton1.Text = "Справочники";
      // 
      // productToolStripMenuItem
      // 
      this.productToolStripMenuItem.Name = "productToolStripMenuItem";
      this.productToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
      this.productToolStripMenuItem.Text = "Продукция";
      this.productToolStripMenuItem.Click += new System.EventHandler(this.продукцияToolStripMenuItem_Click);
      // 
      // departamentToolStripMenuItem
      // 
      this.departamentToolStripMenuItem.Name = "departamentToolStripMenuItem";
      this.departamentToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
      this.departamentToolStripMenuItem.Text = "Подразделение";
      this.departamentToolStripMenuItem.Click += new System.EventHandler(this.подразделениеToolStripMenuItem_Click);
      // 
      // toolStripDropDownButton2
      // 
      this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configRangeToolStripMenuItem});
      this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
      this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
      this.toolStripDropDownButton2.Size = new System.Drawing.Size(80, 22);
      this.toolStripDropDownButton2.Text = "Настройки";
      // 
      // configRangeToolStripMenuItem
      // 
      this.configRangeToolStripMenuItem.Name = "configRangeToolStripMenuItem";
      this.configRangeToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
      this.configRangeToolStripMenuItem.Text = "Временной диапазон";
      this.configRangeToolStripMenuItem.Click += new System.EventHandler(this.rangeConfigToolStripMenuItem_Click);
      // 
      // btnAbout
      // 
      this.btnAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.btnAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnAbout.Image")));
      this.btnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnAbout.Name = "btnAbout";
      this.btnAbout.Size = new System.Drawing.Size(86, 22);
      this.btnAbout.Text = "О программе";
      this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
      // 
      // tabControl
      // 
      this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControl.Location = new System.Drawing.Point(0, 25);
      this.tabControl.Name = "tabControl";
      this.tabControl.SelectedIndex = 0;
      this.tabControl.Size = new System.Drawing.Size(723, 496);
      this.tabControl.TabIndex = 2;
      // 
      // serviceController
      // 
      this.serviceController.ServiceName = "FirebirdServerDefaultInstance";
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(723, 521);
      this.Controls.Add(this.tabControl);
      this.Controls.Add(this.mainMenu);
      this.IsMdiContainer = true;
      this.Name = "MainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Редактор";
      this.Load += new System.EventHandler(this.Form1_Load);
      this.mainMenu.ResumeLayout(false);
      this.mainMenu.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ToolStrip mainMenu;
    private System.Windows.Forms.BindingSource bindingSource1;
    private System.Windows.Forms.TabControl tabControl;
    private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
    private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem departamentToolStripMenuItem;
    private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
    private System.Windows.Forms.ToolStripMenuItem configRangeToolStripMenuItem;
    private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
    private System.Windows.Forms.ToolStripMenuItem productionToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
    private System.Windows.Forms.ToolStripButton btnAbout;
    private System.ServiceProcess.ServiceController serviceController;
  }
}

